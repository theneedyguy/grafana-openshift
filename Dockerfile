FROM centos:7
MAINTAINER kevin.christen@bkw.ch

LABEL name="Grafana" \
      io.k8s.display-name="Grafana" \
      io.k8s.description="Grafana Nightly for use with Prometheus." \
      io.openshift.expose-services="3000" \
      io.openshift.tags="grafana" \
      version=$GRAFANA_VERSION \
      release="1"

# User grafana gets added by RPM
ENV USERNAME=grafana

RUN yum -y update && yum -y upgrade && \
    yum -y install epel-release && \
    yum --disablerepo=epel -y update && \
    yum -y install git unzip nss_wrapper jq wget initscripts urw-fonts
RUN curl -s "https://grafana.com/api/grafana/versions" | jq -r .items[0].version > /tmp/ver
RUN cat /tmp/ver
RUN sed -i s/-/~/g /tmp/ver
RUN cat /tmp/ver
RUN curl "https://dl.grafana.com/oss/main/grafana-$(cat /tmp/ver)-1.x86_64.rpm" > /tmp/grafana.rpm && \
    chmod 777 /tmp/grafana.rpm && \
    rpm -ivh /tmp/grafana.rpm && \
    yum -y clean all && \
    rm /tmp/grafana.rpm

COPY ./root /
 
RUN /usr/bin/fix-permissions /etc/grafana && \
    /usr/bin/fix-permissions /var/log/grafana && \
    /usr/bin/fix-permissions /usr/share/grafana && \
    /usr/bin/fix-permissions /usr/sbin/grafana-server

VOLUME ["/var/lib/grafana", "/var/log/grafana", "/etc/grafana"]

EXPOSE 3000

ENTRYPOINT ["/usr/bin/rungrafana"]
